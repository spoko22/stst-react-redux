const { baseURL } = require('../config.json')

// http://localhost:3000/benefits?country=Italy

export const getBenefitsByCountry = async (countryName) => {
  const res = await fetch(`${baseURL}/benefits?country=${countryName}`)
  return res.json()
}
