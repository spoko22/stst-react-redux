import { generate } from 'shortid'

const { baseURL } = require('../config.json')

// http://localhost:3000/offices?country=Italy

export const getOfficesByCountry = async (countryName) => {
  const res = await fetch(`${baseURL}/offices?country=${countryName}`)
  const offices = await res.json()
  offices.forEach(o => o.id = generate())
  return offices
}
