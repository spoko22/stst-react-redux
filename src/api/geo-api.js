const { baseURL } = require('../config.json')

// http://localhost:3000/geo

export const getGeo = async () => {
  const res = await fetch(`${baseURL}/geo`)
  return res.json()
}
