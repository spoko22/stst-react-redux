const { baseURL } = require('../config.json')

// http://localhost:3000/employees?office_like=Italy

export const getEmployeesByCountry = async (countryName) => {
  const res = await fetch(`${baseURL}/employees?office_like=${countryName}`)
  return res.json()
  // const employees = await res.json()
  // return employees
}
