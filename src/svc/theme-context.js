import React from 'react'

export const themes = {
  red: {
    border: 'red',
    background: '#ffd8d8',
    font: 'brown'
  },
  green: {
    border: 'green',
    background: '#d8ffd8',
    font: 'dark green'
  }
}

export const initialTheme = themes.red

// context: cross-cutting concenrs
// important: reference
export const ThemeContext = React.createContext(initialTheme)

// for dropdown
export const themeOptions = Object.keys(themes)
  .reduce((obj, theme) => {
    obj[theme] = theme
    return obj
  }, {})
