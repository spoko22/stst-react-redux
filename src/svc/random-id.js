export const generateRandomId = () =>
  Math.random() + (new Date()).toISOString()

export const wrapWithRandomId = (value) => ({
  value,
  id: generateRandomId()
})
