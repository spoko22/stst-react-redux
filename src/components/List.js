import React, { memo } from 'react'

import { ItemSeparator } from './styled'

export const List = (propName) =>
  (ItemComponent) => memo(
    ({ collection }) => <div>
      {collection.map(i => {
        const props = { [propName]: i }

        return <ItemSeparator key={i.id}>
          <ItemComponent {...props} />
        </ItemSeparator>
      })}
    </div>
  )
