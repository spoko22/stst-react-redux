import React from 'react'

import { wrapWithRandomId } from '../svc/random-id';
import FruitList from './FruitList';

// propsy są jak input dla komponentu

// functional component
// export const FruitList = ({ label }) => <div>
//   <h2>{label}</h2>
// </div>

// class component
// this.state
// this.props
export class FruitContainer extends React.Component {
  state = {
    fruits: ['banana', 'lemon'].map(wrapWithRandomId),
    display: true, // conditional rendering
    newFruit: ''
  }

  onElementDelete = (fruit) => {
    this.setState({
      fruits: this.state.fruits.filter(f => f !== fruit)
    })
  }

  onElementAdd = () => {
    if (this.state.newFruit.length){
      const fruit = wrapWithRandomId(this.state.newFruit)
      this.setState({
        fruits: [...this.state.fruits, fruit],
        newFruit: ''
      })
    }
  }

  onNewFruitChange = (event) => {
    this.setState({
      newFruit: event.target.value
    })
  }

  render(){
    return <div>
      <h2>{this.props.label}</h2>
      <p>{ this.state.fruits.length } items</p>

      <input type="text"
             value={this.state.newFruit}
             onChange={this.onNewFruitChange}
      ></input>
      <button onClick={this.onElementAdd}>ADD</button>

      {this.state.display &&
        <FruitList
          fruits={this.state.fruits}
          onDelete={this.onElementDelete}
        />}
    </div>
  }
}
