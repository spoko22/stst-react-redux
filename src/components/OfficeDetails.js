import React from 'react'
import CurrencyFormat from 'react-currency-format'

export const OfficeDetails = ({ office: o }) => <div>
  <div>
    {o.country} {o.city}, {o.address}
  </div>
  <div>
    land lord: {o.estate.owner} &nbsp;
    monthly: <CurrencyFormat value={o.estate.monthlyRental} displayType={'text'} thousandSeparator={true} prefix={'$'} /> &nbsp;
  </div>
</div>
