import React from 'react'

import { SemipolarSpinner } from 'react-epic-spinners'

import { ThemeContext } from '../svc/theme-context';

export class Spinner extends React.Component {
  static contextType = ThemeContext

  render() {
    return <SemipolarSpinner size={100} color={this.context.border} />
  }
}
