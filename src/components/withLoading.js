import React from 'react'

import { Spinner } from './Spinner';

export const withLoading = ( WrappedComponent ) =>
  ({ loading, ...props }) =>
    loading ?
    <Spinner /> :
    <WrappedComponent {...props} />

//    <WrappedComponent {...this.props} {...this.state} />

// export const _withLoading = ( WrappedComponent ) =>
//   ({ loading, collection, abc }) =>
//     loading ?
//     <Spinner /> :
//     <WrappedComponent collection={collection} abc={abc} />
