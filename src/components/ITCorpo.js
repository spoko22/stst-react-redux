import React from 'react'

import { Layout } from './styled'
import { ThemedColumn } from './themed'
import { Cost } from './Cost'

import { EmployeeContainer } from './EmployeeContainer'
import { OfficeContainer } from './OfficeContainer'
import { BenefitContainer } from './BenefitContainer'
import { CountryDropdown } from './CountryDropdown'

export const ITCorpo = ({ selectedCountry, costs }) => (<>
  <CountryDropdown />
  {selectedCountry &&
    <div>
      <Cost label="total" amount={costs.total} />
    </div>}
  {selectedCountry && <Layout>
    <ThemedColumn>
      <Cost label="employee" amount={costs.employees} />
      <EmployeeContainer />
    </ThemedColumn>
    <ThemedColumn>
      <Cost label="benefits" amount={costs.benefits} />
      <BenefitContainer />
    </ThemedColumn>
    <ThemedColumn>
      <Cost label="offices" amount={costs.offices} />
      <OfficeContainer />
    </ThemedColumn>
  </Layout>}
</>)

/* Consumer as a render prop:

<ThemeContext.Consumer>
  {(theme) => <>
    <Column theme={theme}>
      <EmployeeContainer />
    </Column>
    <Column theme={theme}>
      <OfficeContainer />
    </Column>
  </>}
</ThemeContext.Consumer>
*/
