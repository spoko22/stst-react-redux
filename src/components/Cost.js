import React from 'react'
import CurrencyFormat from 'react-currency-format'

import { Emphasized } from './styled';

export const Cost = ({ label, amount }) => <Emphasized>
  {label} cost:
  <CurrencyFormat value={amount} displayType={'text'} thousandSeparator={true} prefix={'$'} />
</Emphasized>
