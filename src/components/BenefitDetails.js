import React from 'react'
import CurrencyFormat from 'react-currency-format'

export const BenefitDetails = ({ benefit: b }) => <div>
  <div>
    {b.service}, {b.beneficiary.name}
  </div>
  <div>
    monthly: <CurrencyFormat value={b.monthlyFee} displayType={'text'} thousandSeparator={true} prefix={'$'} /> &nbsp;
    since {b.subscribedAtDate}
  </div>
  <div>
    {b.country}, {b.city}
  </div>
</div>
