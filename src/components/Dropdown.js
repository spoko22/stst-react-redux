import React, { useState } from 'react'

// pseudo quasi nie-do implementacja
// function useState(initial){
//   //.. robota
//   return [currentValue, setValueFunction]
// }

// optionsMap = {
//   k1: v1,
//   k2: v2
// }
export const Dropdown = ({ selected, optionsMap, onOptionChange }) => {
  // const [selected, setSelected] = useState('')

  const handleChange = (e) => {
    const selectedValue = e.target.value // retrieve value from DOM select

    // setSelected(selectedValue) // local state update (consistency)
    onOptionChange(selectedValue) // update parent (callback)
  }

  return (<select value={selected} onChange={handleChange}>
    <option value=""> ? </option>
    {Object.entries(optionsMap).map(([key, value]) =>
      <option key={key} value={key}>{value}</option>
    )}
  </select>)
}
