import { connect } from 'react-redux'

import { compose } from '../utils/fp'

import { withLoading } from './withLoading';
import { List } from './List';
import { BenefitDetails } from './BenefitDetails';

const mapStateToProps = state => ({
  collection: state.benefits.data,
  loading: state.benefits.loading
})

export const BenefitContainer = compose(
  List('benefit'),
  withLoading,
  connect(mapStateToProps)
)(BenefitDetails)
