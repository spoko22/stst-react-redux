import React, { memo } from 'react'

// - props: fruits, onDelete

const FruitList = ({ fruits, onDelete }) => <ul>
  {fruits.map(fruit =>
    <li key={fruit.id}>
      {fruit.value}
      <button onClick={() => onDelete(fruit)}>X</button>
    </li>
  )}
</ul>

// export default FruitList
export default memo(FruitList)
