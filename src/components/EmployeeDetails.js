import React from 'react'
import CurrencyFormat from 'react-currency-format'

export const EmployeeDetails = ({ employee: e }) => <div>
  <div>
    {e.firstName} {e.lastName}, {e.title}
  </div>
  <div>
    contract type: {e.contractType},
    <CurrencyFormat value={e.salary} displayType={'text'} thousandSeparator={true} prefix={'$'} /> &nbsp;
  </div>
  <div>
    account number: {e.account}
  </div>
  <div>
    skills: {e.skills.join(', ')}
  </div>
</div>

// export const EmployeeList =
//   memo(
//     ({ collection }) => <div>
//       {collection.map(i => 
//         <ItemSeparator key={i.id}>
//         <EmployeeDetails employee={i} />
//         </ItemSeparator>
//       )}
//     </div>
//   )
