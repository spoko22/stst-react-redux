import { connect } from 'react-redux'

import { compose } from '../utils/fp'

import { withLoading } from './withLoading';
import { List } from './List';
import { OfficeDetails } from './OfficeDetails';

const mapStateToProps = state => ({
  collection: state.offices.data,
  loading: state.offices.loading
})

export const OfficeContainer = compose(
  List('office'),
  withLoading,
  connect(mapStateToProps)
)(OfficeDetails)
