import { connect } from 'react-redux'

import { withLoading } from './withLoading'
import { Dropdown } from './Dropdown'

import { countryChange } from '../store/actions';

const asCountryNames = (geo) =>
  Object.entries(geo)
    .map(([countryCode, countryName]) => [countryName, countryName])
    .reduce((obj, [key, value]) => {
      obj[key] = value
      return obj
    }, {}) // ES10: Object.fromPairs(pairs)

// kabel na READ
// (state) -> DropdownProps
const mapStateToProps = state => ({
  selected: state.ui.selectedCountry,
  optionsMap: asCountryNames(state.geo.data),
  loading: state.geo.loading // only to support withLoading
})

// kabel na WRITE
const mapDispatchToProps = dispatch => ({
  onOptionChange: (countryName) => {
    dispatch(countryChange(countryName) )
  }
})

export const CountryDropdown =
  connect(mapStateToProps, mapDispatchToProps)(
    withLoading(Dropdown) // <---- (!) that's great // withLoading adds spinner
  )
