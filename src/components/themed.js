import React from 'react'

import { Column } from './styled';
import { ThemeContext } from '../svc/theme-context';

export class ThemedColumn extends React.Component {
  static contextType = ThemeContext

  render(){
    return <Column theme={ this.context }>
      { this.props.children }
    </Column>
  }
}
