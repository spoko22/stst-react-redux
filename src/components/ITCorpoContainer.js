import { connect } from 'react-redux'

import { ITCorpo } from './ITCorpo'

import { totalEmployeeCost, totalOfficeCost, totalBenefitCost, totalCountryCost } from '../store/selectors'

const mapStateToProps = state => ({
  selectedCountry: state.ui.selectedCountry,
  costs: {
    employees: totalEmployeeCost(state),
    offices: totalOfficeCost(state),
    benefits: totalBenefitCost(state),
    total: totalCountryCost(state),
  }
})

export const ITCorpoContainer = connect(mapStateToProps)(ITCorpo)
