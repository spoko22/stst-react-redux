import { connect } from 'react-redux'

import { compose } from '../utils/fp'

import { withLoading } from './withLoading';
import { List } from './List';
import { EmployeeDetails } from './EmployeeDetails';

const mapStateToProps = state => ({
  collection: state.employees.data,
  loading: state.employees.loading
})

export const EmployeeContainer = compose(
  List('employee'),
  withLoading,
  connect(mapStateToProps)
)(EmployeeDetails)
