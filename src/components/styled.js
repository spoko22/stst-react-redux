import styled from 'styled-components'


export const Layout = styled.div`
`

export const Emphasized = styled.span`
  font-weight: bold;
  font-size: 20px;
`

// props:
// - theme ( red | green | ...)
export const Column = styled.div`
  margin: 10px;
  width: 300px;
  border-radius: 8px;
  border: 1px solid ${ props => props.theme.border };
  background-color: ${ props => props.theme.background };
  font: ${ props => props.theme.font };
  box-shadow: 5px 10px 8px ${ props => props.theme.border };
  display: inline-block;
  vertical-align: top;
`

export const ItemSeparator = styled.div`
  padding: 8px;
`
