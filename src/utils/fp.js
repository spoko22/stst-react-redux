export const compose = (...fns) =>
  (value) =>
    fns.reduce((res, fn) => fn(res), value)
