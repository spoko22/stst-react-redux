import { COUNTRY_CHANGE, FETCH_GEO_REQUEST, FETCH_GEO_SUCCESS, FETCH_GEO_FAILURE, FETCH_EMPLOYEES_REQUEST, FETCH_EMPLOYEES_SUCCESS, FETCH_EMPLOYEES_FAILURE, FETCH_OFFICES_REQUEST, FETCH_OFFICES_SUCCESS, FETCH_OFFICES_FAILURE, FETCH_BENEFITS_REQUEST, FETCH_BENEFITS_SUCCESS, FETCH_BENEFITS_FAILURE } from "./constants";

// action creators

export const countryChange = (country) => ({
  type: COUNTRY_CHANGE,
  country
})



export const fetchGeoRequest = () => ({
  type: FETCH_GEO_REQUEST
})

export const fetchGeoSuccess = (response) => ({
  type: FETCH_GEO_SUCCESS,
  response
})

export const fetchGeoFailure = (error) => ({
  type: FETCH_GEO_FAILURE,
  error
})



export const fetchEmployeesRequest = (country) => ({
  type: FETCH_EMPLOYEES_REQUEST,
  country
})

export const fetchEmployeesSuccess = (response) => ({
  type: FETCH_EMPLOYEES_SUCCESS,
  response
})

export const fetchEmployeesFailure = (error) => ({
  type: FETCH_EMPLOYEES_FAILURE,
  error
})



export const fetchOfficesRequest = (country) => ({
  type: FETCH_OFFICES_REQUEST,
  country
})

export const fetchOfficesSuccess = (response) => ({
  type: FETCH_OFFICES_SUCCESS,
  response
})

export const fetchOfficesFailure = (error) => ({
  type: FETCH_OFFICES_FAILURE,
  error
})



export const fetchBenefitsRequest = (country) => ({
  type: FETCH_BENEFITS_REQUEST,
  country
})

export const fetchBenefitsSuccess = (response) => ({
  type: FETCH_BENEFITS_SUCCESS,
  response
})

export const fetchBenefitsFailure = (error) => ({
  type: FETCH_BENEFITS_FAILURE,
  error
})
