export const getResourceReducer = (initialCollection, entity) => {

  const initialState = {
    data: initialCollection,
    loading: false
  }
  
  const resourceReducer = (state = initialState, action) => {
    switch(action.type) {
      case `FETCH_${entity}_REQUEST`:
        return { ...state, loading: true, data: initialCollection }
  
      case `FETCH_${ entity }_SUCCESS`:
        return { ...state, loading: false, data: action.response }
  
      case `FETCH_${ entity }_FAILURE`:
        return { ...state, loading: false }
  
      default:
        return state
    }
  }

  return resourceReducer
}

