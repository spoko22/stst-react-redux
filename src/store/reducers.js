import { combineReducers } from "redux";

import { getResourceReducer } from "./meta-reducers";
import { COUNTRY_CHANGE } from "./constants";


const initialUIState = {
  selectedCountry: null
  // theme
}

const uiReducer = (state = initialUIState, action) => {
  switch (action.type) {
    case COUNTRY_CHANGE:
      return { ...state, selectedCountry: action.country }

    default:
      return state
  }
}

export const rootReducer = combineReducers({
  ui: uiReducer,
  geo: getResourceReducer({}, 'GEO'),
  benefits: getResourceReducer([], 'BENEFITS'),
  offices: getResourceReducer([], 'OFFICES'),
  employees: getResourceReducer([], 'EMPLOYEES'),
})
