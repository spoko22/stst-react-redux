import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension' 

import * as actionCreators from './actions'

import thunk from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'

import runSagas from './sagas'

import { rootReducer } from './reducers'

export const getStore = () => {
  const sagaMiddleware = createSagaMiddleware()

  const middleware = [thunk, sagaMiddleware]

  const store = createStore(rootReducer, composeWithDevTools({
    name: "$$$ IT CORPO $$$",
    actionCreators
  })(
    applyMiddleware(...middleware)
  ))

  runSagas(sagaMiddleware)

  return store
}
