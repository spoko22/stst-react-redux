import {
  fetchGeoRequest, fetchGeoSuccess, fetchGeoFailure,
  fetchEmployeesRequest, fetchEmployeesSuccess, fetchEmployeesFailure,
  fetchOfficesRequest, fetchOfficesSuccess, fetchOfficesFailure,
  fetchBenefitsRequest, fetchBenefitsSuccess, fetchBenefitsFailure,
  countryChange
} from "./actions";

import { getGeo, getEmployeesByCountry, getBenefitsByCountry, getOfficesByCountry } from '../api'

export const fetchGeo = () =>
  (dispatch, getState) => { // getState is also possible
    dispatch( fetchGeoRequest() )
    getGeo()
      .then(geo => dispatch( fetchGeoSuccess(geo) ))
      .catch(error => dispatch( fetchGeoFailure(error) ) )
  }

export const fetchEmployees = (country) =>
  (dispatch) => {
    dispatch(fetchEmployeesRequest(country))
    getEmployeesByCountry(country)
      .then(collection => dispatch(fetchEmployeesSuccess(collection)))
      .catch(error => dispatch(fetchEmployeesFailure(error)))
  }

export const fetchOffices = (country) =>
  (dispatch) => {
    dispatch(fetchOfficesRequest(country))
    getOfficesByCountry(country)
      .then(collection => dispatch(fetchOfficesSuccess(collection)))
      .catch(error => dispatch(fetchOfficesFailure(error)))
  }

export const fetchBenefits = (country) =>
  (dispatch) => {
    dispatch(fetchBenefitsRequest(country))
    getBenefitsByCountry(country)
      .then(collection => dispatch(fetchBenefitsSuccess(collection)))
      .catch(error => dispatch(fetchBenefitsFailure(error)))
  }


const getNext = (collection) => {
  let idx = 0
  return () => {
    const result = collection[idx]
    idx++
    if (idx >= collection.length) {
      idx = 0
    }
    return result
  }
}

export function rotateSelectedCountry(seconds) {
  return function (dispatch, getState) {
    const state = getState()
    const countries = Object.values(state.geo.data)
    const getNextCountry = getNext(countries)

    // initial
    dispatch(countryChange(getNextCountry()))

    setInterval(() => {
      // after N seconds
      dispatch(countryChange(getNextCountry()))
    }, seconds * 1000)
  }
}
