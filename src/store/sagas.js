import { takeLatest, put, select } from 'redux-saga/effects';

import { COUNTRY_CHANGE, FETCH_GEO_SUCCESS } from './constants';

import { fetchEmployees, fetchGeo, fetchOffices, rotateSelectedCountry, fetchBenefits } from './thunks';

function* fetchCountryData() {
  const state = yield select()
  const { selectedCountry } = state.ui
  yield put(fetchEmployees(selectedCountry))
  yield put(fetchOffices(selectedCountry))
  yield put(fetchBenefits(selectedCountry))

  // try {
  //   const titles = yield call(MyTitleModel.get, phrase, licensable);
  //   yield put(fetchTitlesSuccess(titles));
  // } catch (error) {
  //   yield put(fetchTitlesFailure(phrase, error));
  // }
}


function* turnOnRotate() {
  // @ts-ignore (redux-saga doesn't know about redux-thunk)
  yield put(rotateSelectedCountry(5))
}

export function* rotateCountriesAfterGeoLoadedSaga() {
  yield takeLatest(FETCH_GEO_SUCCESS, turnOnRotate)
}


export function* watchCountryChangeSaga() {
  yield takeLatest(COUNTRY_CHANGE, fetchCountryData)
}

export function* bootstrapSaga() {
  // @ts-ignore (redux-saga doesn't know about redux-thunk)
  yield put(fetchGeo())
}

const allSagas = [
  rotateCountriesAfterGeoLoadedSaga,
  bootstrapSaga,
  watchCountryChangeSaga
]
const runSagas = (sagaMiddleware) => allSagas.forEach(saga => sagaMiddleware.run(saga))

export default runSagas
