import { createSelector } from 'reselect'

// selector: (S) -> DATA

const employees = state => state.employees
const offices = state => state.offices
const benefits = state => state.benefits

export const totalEmployeeCost = createSelector(
  employees,
  (employees) => employees.data.reduce( (sum, e) => sum + e.salary , 0 )
)

export const totalOfficeCost = createSelector(
  offices,
  (offices) => offices.data.reduce( (sum, o) => sum + o.estate.monthlyRental, 0 )
)

export const totalBenefitCost = createSelector(
  benefits,
  (benefits) => benefits.data.reduce( (sum, b) => sum + b.monthlyFee, 0 )
)

export const totalCountryCost = createSelector(
  totalEmployeeCost,
  totalOfficeCost,
  totalBenefitCost,
  (employeeCost, officeCost, benefitCost) => employeeCost + officeCost + benefitCost
)
