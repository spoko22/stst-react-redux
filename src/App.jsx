import React, { Component } from 'react';

// standard CSS:
// import './App.module.css';
// CSS Modules:
import appStyles from './App.module.css';

import { ThemeContext, themeOptions, initialTheme, themes } from './svc/theme-context';

import { FruitContainer } from './components/FruitContainer';
import { ITCorpoContainer } from './components/ITCorpoContainer';
import { Dropdown } from './components/Dropdown';

class App extends Component {
  state = {
    theme: initialTheme
  }

  handleThemeChanged = (themeName) => {
    this.setState({ theme: themes[themeName] })
  }

  render() {
    return (
      <ThemeContext.Provider value={this.state.theme}>
        <Dropdown
          optionsMap={themeOptions}
          onOptionChange={ this.handleThemeChanged } />

        <div className={appStyles['App']}>
          <ITCorpoContainer />
          {false && <FruitContainer label={'Fruit List'} />}
        </div>
      </ThemeContext.Provider>
    );
  }
}

export default App;
